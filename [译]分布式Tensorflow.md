# [译]分布式Tensorflow

(本文翻译自**Hands-On Machine Learning with Scikit-Learn and Tensorflow**十二章*Distributing TensorFlow Across Devices and Servers*，侵删。）

在第十一章，我们讨论了一些让训练跑的更快的技巧：更好的权重初始化（*weight initialization*），批量归一（*Batch Normalization*），巧妙的优化算法（optimizers）等等。但是即使使用全部这些技巧，单机训练一个大型神经网络还是需要几天甚至几个星期。

本章我们将看到*tensorflow*如何在多个设备（CPUs和GPUs）分布式并行计算（如图12.1），首先我们将讨论多个设备在一台主机上运行分布式计算。然后再看看多个设备运行在多台主机上。

![11](/img/设备和服务器上的分布式Tensorflow/11.JPG)

支持分布式计算是*tensorflow*对比其他神经网络框架的一大优势。你可以随心所欲的在多个设备和主机间切分（或复制）计算图，通过灵活的并发（parallelize）和同步（synchronize）操作，让人可以轻松尝试不同的并行方法。

我们来看看一些目前并发执行和训练神经网络最流行的方法。不用等训练几周，只要几个小时就能得到结果。这意味着训练不仅能节省大量的时间，还让你有时间尝试更多的模型，并能在得到新数据后重新训练模型。

并发的另一个优势是在*fine-tuning*模型的时候尝试更大的超参集(hyperparameter space)，运行大规模神经网络集合也更加有效率。

不过先’熟‘才能生’巧‘，我们先在单台主机多个GPU上并行运行一些简单的计算图。

## 多个设备，单个主机

在一台主机上增加GPU可以轻松获得巨大的性能提升。事实上在大部分情况下，这样做就足够了，你根本不需要使用多台主机。比如，训练神经网络时，一台8GPU的主机比16个GPU的集群还要快（很多时间浪费在数据在不同的主机间网络传输上了）。

下面我们看看如何设置环境让*Tensorflow*在一台主机多张GPU上运行。接下来我们如何在多个设备上分布式并发执行操作。

### 安装

为了能让*Tensorflow*在多个GPU上运行，首先要确认GPU具备*NVidia Compute Capability*（大于等于3.0）。包含Nvidia's Titan, Titan X, K20和K40（如果你用其他卡，可以访问https://developer.nvidia.com/cuda-gpus确认兼容性）。

`TIPS`

如果没有GPU卡，可以租用*Amazon Aws*上的GPU服务。如何配置请参阅Žiga Avsec’s的[blog](http://max-likelihood.com/2016/06/18/aws-tensorflow-setup/)，谷歌也发布了支持*TensorFlow*运行的[*Cloud Machine Learning*](https://cloud.google.com/ml-engine/)云服务。2016年5月，谷歌发布了使用*tensor processing units*（TPUs）的新平台，能让很多机器学习运行得比在GPU上还要快。当然，最简单的选择就是买一个GPU，Tim Dettmers 有篇[blog](http://timdettmers.com/2017/04/09/which-gpu-for-deep-learning/)介绍如何选择GPU，放心，他更新很勤快呦。

你必须下载安装CUDA和cuDNN库，设置一些环境变量让*Tensorflow*知道哪里可以找到他们。详细的安装指南更新的很频繁，需要上Tensorflow官网查看。

*Nvidia’s Compute Unified Device Architecture library*（CUDA）能让开发者使用支持CUDA的GPU执行各类计算（不止图形加速）。*Nvidia’s CUDA Deep Neural Network library*（cuDNN）是支持深度学习基元的GPU加速库（*a GPU-accelerated library of primitives for DNNs.*）。他优化了很多常见的DNN计算，比如激活层（*activation layers*），归一化（*normalization*），前馈和回馈卷积（*forward and backward*
*convolutions*）还有池化（*pooling*）。这些都是Nvidia’s Deep Learning SDK的一部分（注SDK需要Nvidia开发者账号才能下载）。*Tensorflow*使用CUDA和cuDNN来使用GPU并加快计算速度（如图12-2）。

![22](/img/设备和服务器上的分布式Tensorflow/22.JPG)

可以使用*nvidia-smi*命令查看*CUDA*是否正确按章。该命令会列出可用的GPU并显示在GPU上运行的进程。

![33](/img/设备和服务器上的分布式Tensorflow/33.JPG)

最后，需要安装支持GPU版本的*Tensorflow*，如果你是用*virtualenv*创建一个隔离的画家，首先要激活他：

```bash
$ cd $ML_PATH # Your ML working directory (e.g., $HOME/ml)
$ source env/bin/activate
```

接下来安装支持GPU版本的Tensorflow：

```bash
$ pip3 install --upgrade tensorflow-gpu
```

现在可以打开一个*Python shell*查看Tensorflow是否发现CUDA和cuDNN了，通过导入*tensorflow*创建*session*查看：

```python
>>> import tensorflow as tf
I [...]/dso_loader.cc:108] successfully opened CUDA library libcublas.so locally
I [...]/dso_loader.cc:108] successfully opened CUDA library libcudnn.so locally
I [...]/dso_loader.cc:108] successfully opened CUDA library libcufft.so locally
I [...]/dso_loader.cc:108] successfully opened CUDA library libcuda.so.1 locally
I [...]/dso_loader.cc:108] successfully opened CUDA library libcurand.so locally
>>> sess = tf.Session()
[...]
I [...]/gpu_init.cc:102] Found device 0 with properties:
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I [...]/gpu_init.cc:126] DMA: 0
I [...]/gpu_init.cc:136] 0: Y
I [...]/gpu_device.cc:839] Creating TensorFlow device
(/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
```

OK，*Tensorflow*发现了CUDA和cuDNN库，用这两个库发现了GPU（本例中使用了Nvidia Grid K520）。

### 管理GPU内存

默认情况下，当第一次运行计算图的时候，Tensorflow会直接占据所有可用GPU的内存，所以第一个任务不结束，后面的Tensorflow任务无法执行。如果这时候你运行第二个，会得到如下错误：

![77](/img/设备和服务器上的分布式Tensorflow/77.JPG)

一个解决办法是每一个GPU指定不同的任务。为了达到这个目的，最简单的方法是设置环境变量**CUDA_VISIBLE_DEVICES**，这样每个进程只能看到指定的GPU，例如你可以启动两个程序如下：

```bash
$ CUDA_VISIBLE_DEVICES=0,1 python3 program_1.py
# and in another terminal:
$ CUDA_VISIBLE_DEVICES=3,2 python3 program_2.py
```

程序#1只能看到GPU0和 1，程序#2只能看到GPU2和3。完美解决了问题。如图12-3.

![99](/img/设备和服务器上的分布式Tensorflow/99.JPG)

另外一个办法是告诉*Tensorflow*只使用一部分内存。比如为了让每个*TensorFlow*程序只占用40%的GPU内存，需要创建一个*ConfigProto*对象，设置**gpu_options.per_process_gpu_memory_fraction**为0.4，用这个配置创建Session：

```python
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
session = tf.Session(config=config)
```

现在两个程序可以并行在同一个GPU上了（但是三个不行，因为3*0.4>1)，如图12-4。

![100](/img/设备和服务器上的分布式Tensorflow/100.JPG)

当两个程序同时运行运行*nvidia-smi*命令时，能看到每个程序占大概40%内存：

![101](/img/设备和服务器上的分布式Tensorflow/101.JPG)

还有一个办法时让TensorFlow需要内存的时候再申请。设置**config.gpu_options.allow_growth**为True可以做到。但是TensorFLow占到内存后就不会释放了（防止内存碎片），所以还有可能跑着跑着OOM了。这个选项会有什么表现无法预判，所以最好还是用前面两个选择。

OK，现在我们有了一个支持GPU的能干活的TensorFlow，下面看看如何使用他。

### 在设备上指定操作（Placing Operations on Devices）

TensorFlow白皮书上描述了一种易用的动态占位器算法，可以动态的在所有可用设备上执行分布操作，记录各类数据，例如计量上次执行计算图的时间，评估每种运算的输入输出大小，每个设备可用内存大小，不同设备间传输数据的延迟时间，用户设置的提示（*hint*）和限制（*constraints*）等等。不行的是，如此精致的算法只流传于Google内部，并没有发布在开源版TensorFlow上。原因是实际操作中发现，用户设置少量的规则会让运算比更高效。不过*TensorFlow*团队正在努利提升动态占位器的内力，也有未来会发不出来。

目前TensorFlow正是依赖于这些简易的占位器（正如他的名字暗示的），这些都很基本

**简单的占位（Simple placement）**

当运行一个图（graph），如果*TensorFlow*正要算出一个没有分配在设备上的节点（*node*），*他*会用简单占位器占住，简单占位器（simple placer）服从如下规则：

- 如果该节点已经在上一轮图计算中放在一个设备上了，他会留在该设备上。
- 如果用户指定了该节点的设备（下面介绍），占位器会安排在这个设备
- 上了两个都不满足，默认是GPU #0， 没有GPU就放在CPU #0

如你所见，把操作放在合适的设备完全由你决定。如果你什么都不想做，整个计算图会放置在默认的设备上。想把节点放在指定设别，必须要用*device()*方法创建设备块（*a device block*）。比如，下面代码会将变量`a`和常量`b`放在CPU上，乘法节点`c`没有指定设备，他会被放在默认设备上：

```python
with tf.device("/cpu:0"):
    a = tf.Variable(3.0)
    b = tf.constant(4.0)
c = a * b    
```

`TIPS`：“**/cpu:0**”是指多CPU系统的所有CPU，目前无法在指定CPU或CPU群组上执行命令。

**记录位置（Logging placement）**

现在我们检测一下上文定义的简单占位器的约束规则。可以设置**log_device_placement**为True；这会让TensorFlow设置节点时打出一条信息，如下：

```python
>>> config = tf.ConfigProto()
>>> config.log_device_placement = True
>>> sess = tf.Session(config=config)
I [...] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520,
pci bus id: 0000:00:03.0)
[...]
>>> x.initializer.run(session=sess)
I [...] a: /job:localhost/replica:0/task:0/cpu:0
I [...] a/read: /job:localhost/replica:0/task:0/cpu:0
I [...] mul: /job:localhost/replica:0/task:0/gpu:0
I [...] a/Assign: /job:localhost/replica:0/task:0/cpu:0
I [...] b: /job:localhost/replica:0/task:0/cpu:0
I [...] a/initial_value: /job:localhost/replica:0/task:0/cpu:0
>>> sess.run(c)
12
```

以*I*开头的行为日志信息。当我们创建session时，TensorFlow会打一条日志，告诉我们他找到GPU（我这是Grid K520）。当我们第一次运行计算图时（本例中是初始化变量*a*），简单占位器会将会将节点放在指定的设备上。正如期待，日志信息显示所有节点都放在了“**/cpu:0**”上，除了乘法节点被放在“**/gpu:0**”上（现在可以忽略**/job:localhost/replica:0/task:0**前缀，等会会提到这些）。注意第二次我们计算`c`，占位器没有启动，因为TensorFlow里所有要计算*c*节点都已经分配好了。

**动态放置方法（Dynamic placement function）**

创建设备块时，可以写个方法而不用直接用设备名。TensorFlow会在执行操作指定设备时先调用该方法。这个方法必须返回操作要执行的设备名称。比如，下面代码会将所有变量节点（variable nodes）设置在"**/cpu:0**"上，其他节点在"**/gpu:0**"：

```python
def variables_on_cpu(op):
    if op.type == "Variable":
        return "/cpu:0"
    else:
        return "/gpu:0"
with tf.device(variables_on_cpu):
    a = tf.Variable(3.0)
    b = tf.constant(4.0)
    c = a * b
```

你可以很容易的实现更复杂的算法，比如用轮询的方式分配变量（pinning variables across
GPUs in a round-robin fashion）。

**操作和内核（Operations and kernels）**

TensorFlow想运行在一台设备上，他需要有对这台设备的实现，这就叫做内核（kernel）。很多操作对CPU和GPU都有内核，但不是全部哦，比如，整数变量在GPU内核上就没实现，下面想要将变量 **i** 放在GPU #0上的代码会失败：

```python
>>> with tf.device("/gpu:0"):
... i = tf.Variable(3)
[...]
>>> sess.run(i.initializer)
Traceback (most recent call last):
[...]
tensorflow.python.framework.errors.InvalidArgumentError: Cannot assign a device
to node 'Variable': Could not satisfy explicit device specification
```

注意TensorFlow推断这个变量时`int32`类型，因为初始化值是个整型。如果把初始化值改为3.0，或者创建变量时显式设置`dytpe=tf.float32`，就没问题了。

**身段放软的放置（Soft placement）**

默认情况下，把操作钉在内核不支持的设备，前面描述的放置失败异常就会抛出来。如果你想要TensorFlow退回放在CPU上，可以设置选项`allow_soft_placement`为True：

```python
with tf.device("/gpu:0"):
    i = tf.Variable(3)

config = tf.ConfigProto()
config.allow_soft_placement = True
sess = tf.Session(config=config)
sess.run(i.initializer) # the placer runs and falls back to /cpu:0
```

目前我们讨论了如何在不同设备上放置节点。下面我们看看TensorFlow如何让这些节点并发运行。

### 并发执行（Parallel Execution）

TensorFlow运行计算图时，首先找出所有想要计算的节点，然后计算他们之间有多少依赖。接着TensorFlow开始计算零依赖的节点（例如数据源节点`source nodes`）。如果这些节点运行在不同设备上，显然他们会并发计算。如果是在同一设备不同线程中计算，这些节点也会并发运行（GPU多线程或CPU多核）。

TensorFlow为每个设备准备一个线程池用于并发操作（如图12-5）。这些被称为操作间线程池（the  inter-op thread pool）。一些操作有多线程内核：他们可以使用其他线程池（一个设备一个），这被叫做操作间线程池。（*Some operations have multithreaded kernels: they can use other thread pools (one per device) called the intra-op thread pools.*）（不知道你懂没，反正我没懂）

![102](/img/设备和服务器上的分布式Tensorflow/102.JPG)

如图12-5，**A**, **B**和**C**是源操作，他们可以立即执行。操作**A**和**B**放在GPU#0上，所以他们可以送到设备的操作间线程池（*inter-op thread pool*），立刻就能并发计算了。操作*A*正好有个多线程内核；计算集就被分成三部分，好在线程池中并发执行。操作**C**运行在GPU#1的操作间线程池上。

操作**C**完成后，操作**D**和**E**的依赖计数器都减到0，所以两个操作会发到线程池中执行。

`TIPS`：通过设置`inter_op_parallelism_threads`，你可以控制操作间线程池中线程的数量。注意是第一个session的时候创建操作间线程池。其他所有session会重用这些线程除非设置`use_per_session_threads`为True。你可以通过设置`intra_op_parallelism_threads`为True控制每个操作间线程池中线程的数量。

### Control Dependencies

有些情况下，可能需要延迟执行特定操作，即使他的所有依赖操作都执行完了，也要延迟执行。例如，这个操作需要很大内存但他的值很后面才需要。最好能最后再执行这个操作，省的占了很多内存影响其他操作运行。另一个例子是一个依赖设备外部数据的操作集（*a set of operations*），如果他们同时执行，会占满带宽，还会一起等待IO。其他需要传输数据的操作也被阻挡。最好能顺序的执行这些需要大量数据传输的操作，让设备能并发的执行其他操作。

想要延迟执行一些节点，一个简单方法就是使用操作依赖（*control dependencies*）。比如，下面的代码告诉TensorFlow等**a**和**b**算好了再计算**x**：

```python
a = tf.constant(1.0)
b = a + 2.0
with tf.control_dependencies([a, b]):
    x = tf.constant(3.0)
    y = tf.constant(4.0)

z = x + y 
```

显然，因为**z**依赖**x**和**y**，虽然没有显示使用*control_dependencies()*，**z**也要等**a**和**b**算好再执行。因为**b**其实也依赖**a**，我可以只写依赖**b**而不是**[a, b]**，不过有些时候“显式好于隐式”。

好啦！现在我们知道：

- 如何在多个设备中指定操作
- 如何让操作并发执行
- 如何通过依赖控制优化并发执行

现在是时候讨论如何在多个主机上分布式计算了！

## 多个主机，多个设备（Multiple Devices Across Multiple Servers）

想要在多个主机上运行计算图，首先要定义集群。集群就是一组TensorFlow主机，任务被分配在不同主机上（如图12-6）。每个任务属于一个job，一个job其实就是一组有名字有角色的任务集。比如跟踪模型参数（这种job通常被叫做"**ps**", *parameter server*的简称），或者用于计算（这中job通常被称为“**worker**”）。

![103](/img/设备和服务器上的分布式Tensorflow/103.JPG)

下面的集群配置定义了两个job，“**ps”**和“**worker”**，分别有一个任务和两个任务。这个例子中，**A**机器上有两个TensorFlow服务器，监听不容的端口，一个是运行部分的“**ps**” job，另一个运行部分的“**worker**” job， **B**机器上只有一个TenserFlow服务器， 运行部分的“**worker**” job。

```python
cluster_spec = tf.train.ClusterSpec({
    "ps": [
        "machine-a.example.com:2221", # /job:ps/task:0
    ],
    "worker": [
        "machine-a.example.com:2222", # /job:worker/task:0
        "machine-b.example.com:2222", # /job:worker/task:1
]})
```

想要启动TenserFlow服务器，必须先创建**Server**对象，并传入集群配置（这样才能和其他服务器通信），job名字和任务号，比如，想启动第一个**worker**任务，机器**A**的代码如下：

```python
server = tf.train.Server(cluster_spec, job_name="worker", task_index=0)
```

正常情况下一台机器一个任务这样更简单，但是上面的例子说明，只要你想，让TensorFlow在同一台机器上运行多个任务也行。如果一台机器上运行多个服务器，正如前面所说，必须保证这些任务不要把所有GPU的内存用光。例如，图12-6的“**ps**”任务看不到GPU，因为进程以`CUDA_VISIBLE_DEVICES=""`启动。注意CPU是可以被同台机器所有任务共享的。

如果你希望这个进程除了运行TensorFlow服务器啥也不做，可以通过Server对象的join()方法让server等待（要不然等主线程结束推出服务器立刻被关掉）。因为目前没有办法关掉服务器，服务器会一直阻塞：

```python
server.join() # blocks until the server stops (i.e., never)
```

### 打开一个Session

等所有任务启动并运行（还没做事）后，客户端可以在需要的服务器上打开**session**，可以定位（**located**）在任何机器任何进程（甚至一个正在运行任务的进程），使用**session**方式和本地session一样。例如：

```python
a = tf.constant(1.0)
b = a + 2
c = a * 3

with tf.Session("grpc://machine-b.example.com:2222") as sess:
    print(c.eval()) # 9.0
```

客户段代码创建一个简单的计算图，在主机**B**的TensorFlow服务器上打开Session（我们叫他**Master**主机），计算**c**值。Master主机会为运算选择合适的设备。本例中，因为我们没有指定设备，Master会使用默认的设备，在这里就事主机**B**的GPU。GPU拿到运算图，计算结果返回。

### Master和Worker服务（The Master and Worker Services）

客户端使用**gRPC**协议（*Google Remote Procedure Call*）和服务器通信。这事一个跨平台，跨语言，开源高效的远程调用框架。该框架基于HTTP2协议，连接建立好后会一直保持高效的双向连接。数据会以*protocol buffers*协议传输，这是另外一个*Google*开源的轻量级二进制数据交换格式。

`TIPS`：TensorFLow集群中的所有服务器都有可能和该集群内的其他服务器通信，务必确认防火墙打开。

每个TensorFlow服务器提供两个服务：*master*服务和*worker*服务。*master*服务可以打开**sessions**和运行计算图。还负责协调不同任务间的计算，依靠*worker*执行其他任务上实际的计算并得到结果（翻译不好这句：The master service allows clients to open sessions and use them to run graphs. It
coordinates the computations across tasks, relying on the worker service to actually
execute computations on other tasks and get their results）

这种构架给了人很大的灵活度。一个客户端可以在多个服务器不同线程上开多个sessions。一个服务器可以同时处理一个或多个客户端session。你可以每一个客户端运行一个任务（典型的同一个进程内），或者一个客户端控制所有任务，所有用法统统支持。

### 把操作钉在任务上（Pinning Operations Across Tasks）

通过指定任务名，任务索引，设备类型和设备索引，你可以用设别块把操作订在任意任务管理下的任意设备上。例如，下面的代码会把**a**钉在“**ps**”job第一个任务的CPU上（CPU在机器A上），**b**会钉在“**worker**” job第一个任务的第二个GPU上（GPU #1 在机器A上）。最后，**c**没有钉上任何设备，master会把他放在默认的设备上（机器**B**的GPU）。

```python
with tf.device("/job:ps/task:0/cpu:0")
    a = tf.constant(1.0)
with tf.device("/job:worker/task:0/gpu:1")
    b = a + 2
c = a + b
```

之前如果你忽略设备类型和索引，TensorFlow会把该任务默认的设备分给他；比如，把操作分给“**/job:ps/task:0**”会将操作分到“**ps**” job第一个任务的默认设备（主机A的CPU）。如果你忽略了任务索引（比如“**/job:ps**”）,TensorFlow会默认在"**/task:0**"。如果忽略了job名字和任务索引，TensorFlow会默认在session的master任务。

### 变量分片部署在多个参数服务器（Sharding Variables Across Multiple Parameter Servers）

我们马上就能看到，分布式训练神经网络的通用模式是把模型参数放在一组参数服务器上（例如**ps** job的任务），而其他任务专注在计算上（例如**worker** job的任务）。对于有百万参数的大型模型，将这些参数分片放在多个参数服务器上非常有用，因为这样可以降低单台机器网卡传输饱和的风险。如果是手动的将参数分别放在不同参数服务器上，这会非常麻烦。TensorFlow提供了的方法`replica_device_setter()`，可以通过循环的方式把变量分配在**ps**任务上。比如，下面代码把五个变量分配在两个参数服务器上：

```python
with tf.device(tf.train.replica_device_setter(ps_tasks=2):
    v1 = tf.Variable(1.0) # pinned to /job:ps/task:0
    v2 = tf.Variable(2.0) # pinned to /job:ps/task:1
    v3 = tf.Variable(3.0) # pinned to /job:ps/task:0
    v4 = tf.Variable(4.0) # pinned to /job:ps/task:1
    v5 = tf.Variable(5.0) # pinned to /job:ps/task:0
```

不用传入**ps**任务的数目，直接传集群规范**cluster=cluster_spec**，TensorFlow会自己计算ps job中的任务数量。

如果代码块内不止变量，还要构建其他操作，TensorFlow会自动把这些钉到“**/job:worker**”上，就是“worker” job下默认的第一个任务的第一块设备。你也可以通过设置`worker_device`参数将操作钉到其他的设备。但是更好的办法是使用嵌入设备代码块，内部的代码块会覆盖外部的job，task，device定义。比如：

```python
with tf.device(tf.train.replica_device_setter(ps_tasks=2)):
    v1 = tf.Variable(1.0) # pinned to /job:ps/task:0 (+ defaults to /cpu:0)
    v2 = tf.Variable(2.0) # pinned to /job:ps/task:1 (+ defaults to /cpu:0)
    v3 = tf.Variable(3.0) # pinned to /job:ps/task:0 (+ defaults to /cpu:0)
    [...]
    s = v1 + v2 # pinned to /job:worker (+ defaults to task:0/gpu:0)
    with tf.device("/gpu:1"):
        p1 = 2 * s # pinned to /job:worker/gpu:1 (+ defaults to /task:0)
        with tf.device("/task:1"):
            p2 = 3 * s # pinned to /job:worker/task:1/gpu:1
```

`TIPS`：本例中假设参数服务器只使用CPU，这是典型的用法。因为这里只需要保存和通信，不需要密集计算。

### 使用资源服务器跨越Session共享状态（Sharing State Across Sessions Using Resource Containers）

当你使用普通的本地session（不是分布那种），每个变量状态时session自己保存的；只要一结束所有变量就丢了。此外，多个session，即使运行同一个图，也不能共享任何状态；每个session都有一份所有变量的拷贝（第九章讨论过）。相对比的，如果使用分布式sessions，变量状态被集群中的资源服务器保存，不放在session里。所以如果在一个客户端session中创建**x**变量，这个变量会自动在同集群中所有session中生效（甚至是连接在不同机器上的session）。比如，参考如下代码：

```python
# simple_client.py
import tensorflow as tf
import sys
x = tf.Variable(0.0, name="x")
increment_x = tf.assign(x, x + 1)
with tf.Session(sys.argv[1]) as sess:
    if sys.argv[2:]==["init"]:
        sess.run(x.initializer)
    sess.run(increment_x)
    print(x.eval())
```

假设我们的TensorFlow集群运行在机器A和B上，端口2222。你可以运行客户端，在主机**A**服务上打开session，初始化变量，x加1，打印结果，命令如下：

```bash
$ python3 simple_client.py grpc://machine-a.example.com:2222 init
1.0
```

现在如果你执行下面命令，连接主机**B**的服务。她会重用同一个变量**x**（这次我们不用让服务器初始化变量了）：

```bash
$ python3 simple_client.py grpc://machine-b.example.com:2222
2.0
```

这个功能是双刃剑：如果你想要在不同session中共享变量那好极了，但是如果你想在同一个集群中完全独立的计算，那就必须注意不要错用同一个名字了。有个保证不会名字冲突的办法是把构造部分放到一个唯一的变量范围（`variable scope`）中，例如：

```python
with tf.variable_scope("my_problem_1"):
    [...] # Construction phase of problem 1 
```

用容器块是个更好的方法：

```python
with tf.container("my_problem_1"):
    [...] # Construction phase of problem 1
```

这是使用一个专用容器去解决问题#1， 代替默认的容器（名字是空字符串“”）。这样做的一个有点事变量名称短而美，另一个优点事很容易重置变量。比如，下面的命令会连接机器A的服务器，并让他重置名字为”my_problem_1"的容器。即释放容器内使用的所有资源（同时关闭服务器上所有session）。这个容器管理的所有变量如果需要再使用，都必须重新初始化。

```python
tf.Session.reset("grpc://machine-a.example.com:2222", ["my_problem_1"])
```

使用资源服务器可以灵活的方式让session间更容易共享变量。比如，图12-7是在同一个集群内运行不同的图，但有一些共享变量。客户端A和B共享同一个由默认容器管理的变量**x**，而客户端C和D共享容器**my_problem_1**管理另一个变量**x**。注意客户端C使用了两个容器的变量。

![104](/img/设备和服务器上的分布式Tensorflow/104.JPG)

资源服务器（*Resource containers*）也可以保存其他有状态的操作，带名字的队列还有阅读器。让我们先看看队列。

### 使用TensorFlow队列进行异步通信（Asynchronous Communication Using TensorFlow Queues）

队列是另外一个很棒的多session间交互数据方式；例如，一个常见用例是一个客户端创建图，加载训练数据然后放进队列，而另一个客户端创建图，从队列中取出数据，训练模型。这样做可以明显加快训练速度，因为训练操作不需要每一步都等待下一个mini-batch。

![105](/img/设备和服务器上的分布式Tensorflow/105.JPG)

TensorFlow提供多种队列。最简单的一种是先进先出（`FIFO`）队列。例如，下面的代码创建FIFO队列，可以放置长度为10的张量，每个张量里有两个浮点值：

```python
q = tf.FIFOQueue(capacity=10, dtypes=[tf.float32], shapes=[[2]],
    name="q", shared_name="shared_q")
```

`TIPS`：想要跨session共享变量，你需要做的是两边都使用同样名字的容器和变量。队列里TensorFlow不用**name**属性而是用**share_name**，所以指定他很重要（即使他和**name**一样）。当然，还要用同样的容器。

**Enqueuing data**

推送数据入队列，必须创建入队（*enqueue*）操作。比如，下面的代码推送三个训练实例进入队列：

```python
# training_data_loader.py
import tensorflow as tf
q = [...]
training_instance = tf.placeholder(tf.float32, shape=(2))
enqueue = q.enqueue([training_instance])
with tf.Session("grpc://machine-a.example.com:2222") as sess:
    sess.run(enqueue, feed_dict={training_instance: [1., 2.]})
    sess.run(enqueue, feed_dict={training_instance: [3., 4.]})
    sess.run(enqueue, feed_dict={training_instance: [5., 6.]})
```

也可以一次入队多个操作，而不是一个一个的入队：

```python
[...]
training_instances = tf.placeholder(tf.float32, shape=(None, 2))
enqueue_many = q.enqueue([training_instances])
with tf.Session("grpc://machine-a.example.com:2222") as sess:
    sess.run(enqueue_many,
        feed_dict={training_instances: [[1., 2.], [3., 4.], [5., 6.]]})
```

两个例子都是推送三个张量进入队列。

`Dequeuing data`

从队列另一端取出实例，需要使用*dequeue*操作：

```python
# trainer.py
import tensorflow as tf
q = [...]
dequeue = q.dequeue()
with tf.Session("grpc://machine-a.example.com:2222") as sess:
    print(sess.run(dequeue)) # [1., 2.]
    print(sess.run(dequeue)) # [3., 4.]
    print(sess.run(dequeue)) # [5., 6.]
```

一般来说，如果想一次取出整个*mini-batch*，而不是一次取一个。需要使用*dequeue_many*操作。指定*mini-batch*大小：

```python
[...]
batch_size = 2
dequeue_mini_batch= q.dequeue_many(batch_size)
with tf.Session("grpc://machine-a.example.com:2222") as sess:
    print(sess.run(dequeue_mini_batch)) # [[1., 2.], [4., 5.]]
    print(sess.run(dequeue_mini_batch)) # blocked waiting for another instance
```

队列满的时候，入队操作会阻塞直到队列里内容被取出。相似的，当队列为空（或者使用*dequeue_many()*方法但是队列里内实例数量小于*mini-batch*大小），出队操作会阻塞直到足够多的实例被推送进队列。

**元组队列（Queues of tuples）**

队列内的元素不单单可以是单一张量，还可以是张量元组（*a tuple of tensors*）（各种类型和形状）。比如，下面的队列存储张量对，一个是int32类型，形状是（）。另一个是float32，形状是[3, 2]：

```python
q = tf.FIFOQueue(capacity=10, dtypes=[tf.int32, tf.float32], shapes=[[],[3,2]],
        name="q", shared_name="shared_q") 
```

能够入队必须是成对的张量（注意每对张量作为队列里的一个对象）：

```python
a = tf.placeholder(tf.int32, shape=())
b = tf.placeholder(tf.float32, shape=(3, 2))
enqueue = q.enqueue((a, b))
with tf.Session([...]) as sess:
    sess.run(enqueue, feed_dict={a: 10, b:[[1., 2.], [3., 4.], [5., 6.]]})
    sess.run(enqueue, feed_dict={a: 11, b:[[2., 4.], [6., 8.], [0., 2.]]})
    sess.run(enqueue, feed_dict={a: 12, b:[[3., 6.], [9., 2.], [5., 8.]]})
```

另外一边，*dequeue()*方法会将一对张量出队：

```python
dequeue_a, dequeue_b = q.dequeue()
```

接着上面，应该执行如下操作出队：

```python
with tf.Session([...]) as sess:
    a_val, b_val = sess.run([dequeue_a, dequeue_b])
    print(a_val) # 10
    print(b_val) # [[1., 2.], [3., 4.], [5., 6.]]
```

`TIPS`：如果只运行*dequeue_a*，那只会返回第一个元素，第二个元素会丢（相似的，如果只计算*dequeue_b*，第一个元素会丢）。

*dequeue_many()*方法同样也会成对返回：

```python
batch_size = 2
dequeue_as, dequeue_bs = q.dequeue_many(batch_size)
```

看看运行结果：

```python
with tf.Session([...]) as sess:
    a, b = sess.run([dequeue_a, dequeue_b])
    print(a) # [10, 11]
    print(b) # [[[1., 2.], [3., 4.], [5., 6.]], [[2., 4.], [6., 8.], [0., 2.]]]
    a, b = sess.run([dequeue_a, dequeue_b]) # blocked waiting for another pair
```

**关闭队列**

可以关闭队列来告诉其他session，不能再让数据入队了：

```python
close_q = q.close()
with tf.Session([...]) as sess:
    [...]
    sess.run(close_q)
```

接下来再运行*enqueue*或*enqueue_many*会抛出异常。默认情况下，还在挂起的入队请求会先被处理，除非你调用方法（*q.close(cancel_pending_enqueues=True)*）。

只要队列里还有元素没有处理，接下来的*dequeue*或*dequeue_many operations*操作会继续执行。但是如果队列内没有足够元素，方法会失败。如果用*dequeue_many*操作，但是队列中元素比*mini-batch*数量小，这些数据会丢失。更推荐使用*dequeue_up_to*操作；他的表现和*dequeue_many*一样，但是当队列被关闭而队列中的数据数量小于mini-batch时，*dequeue_up_to*会正常返回他们。

**RandomShuffleQueue**

TensorFlow还支持其他很多类型的队列，比如**RandomShuffleQueue**，这个队列和**FIFOQueue**表现一样，但会以随机顺序出队。在训练过程中这个队列很有用，他可以帮你在每个epoch打乱数据顺序读出。首先，我们创建队列：

```python
q = tf.RandomShuffleQueue(capacity=50, min_after_dequeue=10,
        dtypes=[tf.float32], shapes=[()],
        name="q", shared_name="shared_q")
```

*min_after_dequeue*参数代表最少要多少个元素必须留在队列中。他确保有队列中的元素足够随机（一旦队列被关，这个参数*min_after_dequeue*限制被忽略）。假设向队列中传入22个元素（float 1到22）.下面时dequeue的表现：

```python
dequeue = q.dequeue_many(5)
with tf.Session([...]) as sess:
    print(sess.run(dequeue)) # [ 20. 15. 11. 12. 4.] (17 items left)
    print(sess.run(dequeue)) # [ 5. 13. 6. 0. 17.] (12 items left)
    print(sess.run(dequeue)) # 12 - 5 < 10: blocked waiting for 3 more instances
```

**PaddingFifoQueue**

*PaddingFIFOQueue*也可以当**FIFOQueue**用，不过他可以接收任何尺寸的张量变量大小（要有固定的秩）。这时dequeue_many或dequeue_up_to操作会返回同样尺寸的张量，这个尺寸会是这次mini-batch中最大的张量的大小，其他张量不足的不会用0填充。比如，可以传入大小随意的二维张量（矩阵）：

```python
q = tf.PaddingFIFOQueue(capacity=50, dtypes=[tf.float32], shapes=[(None, None)]
    name="q", shared_name="shared_q")
v = tf.placeholder(tf.float32, shape=(None, None))
enqueue = q.enqueue([v])
with tf.Session([...]) as sess:
    sess.run(enqueue, feed_dict={v: [[1., 2.], [3., 4.], [5., 6.]]}) # 3x2
    sess.run(enqueue, feed_dict={v: [[1.]]}) # 1x1
    sess.run(enqueue, feed_dict={v: [[7., 8., 9., 5.], [6., 7., 8., 9.]]}) # 2x4
```

如果一次只出队一个元素，那他的大小和入对的一样。但是如果一次出队几个元素（用*dequeue_many()*或*dequeue_up_to()*），队列会自动填充。比如，如果一次出列三个张量，这三个都会用0填充变成3*4张量。因为第一维最大长度是3（第一个张量），最大第二维长度是4（第三个张量）：

```python
>>> q = [...]
>>> dequeue = q.dequeue_many(3)
>>> with tf.Session([...]) as sess:
...     print(sess.run(dequeue))
[[[ 1. 2. 0. 0.]
[ 3. 4. 0. 0.]
[ 5. 6. 0. 0.]]

[[ 1. 0. 0. 0.]
[ 0. 0. 0. 0.]
[ 0. 0. 0. 0.]]

[[ 7. 8. 9. 5.]
[ 6. 7. 8. 9.]
[ 0. 0. 0. 0.]]]
```

这种类型的队列，在处理变长输入的问题上有用，列入文字序列（看十四章）。

好了，现在我们暂停一下：到现在为止我们学习了多机多设备分布计算，跨session共享变量，利用队列异步通信，在我们开始训练神经网络前，让我们讨论一下最后一个主题：如何有效的加载训练数据。

### 直接从图中加载数据（Loading Data Directly from the Graph）

目前为止我们假设客户端用占位符（*placeholders*）加载训练数据并喂给集群。对于简单设置这非常有效，但是其实这非常低效，因为要传输数据好几次：

1. 从文件系统到客户端。
2. 从客户端到master任务。
3. 有需要的话还要从master任务到其他任务。

如果同样的训练数据通过多个客户端训练神经网络（比如超参调优），那会更糟糕：如果每个客户端同步加载数据，你可能会面对文件系统IO饱和或者网络饱和。

**预加载数据到变量（Preload the data into a variable）**

对于可以可以加载进内存的数据集，更好的选项是加载一次然后分给一个变量，然后直接在图里用这个变量就好了。这叫做预加载训练数据。这种方法从客户端到集群数据只会传输一次（但根据操作需求，还有可能在任务间传输）。下面代码展示如何加载整个训练集到变量：

```python
training_set_init = tf.placeholder(tf.float32, shape=(None, n_features))
training_set = tf.Variable(training_set_init, trainable=False, collections=[],
name="training_set")

with tf.Session([...]) as sess:
    data = [...] # load the training data from the datastore
    sess.run(training_set.initializer, feed_dict={training_set_init: data})
```

必须设置*trainable=False*告诉优化器不用改这个变量的值。也必须设置*collections=[]*保证变量不会被添加到*GraphKeys.GLOBAL_VARIABLES*集合，这是用来保存和恢复检查点（*checkpoints*）。

`TIPS`：这个例子假设所有训练数据都是`float32`，如果不是这样，每种类型一个变量。

**直接从图里读训练数据（Reading the training data directly from the graph）**

如果内存容不下这么大的数据集，一个好的方案是使用*reader*操作：他有能力直接从文件系统读数据。使用这个办法训练数据不会在客户端间产生数据流动。TensorFlow支持多种格式数据：

- CSV
- 定长二进制记录（*Fixed-length binary records*）
- TensorFlow自己的*TFRecords*格式，基于*protocol buffers*协议

我们看个读取CSV文件的简单例子（其他格式请看API文档）。假设你有个保存训练数据的文件，名字叫*my_test.csv*，你想创建操作去读他。假设文件内容如下，两个*float*特征*x1*和*x2*，是整型，目标类型是二分类（binary class）的整型数据：

```javascript
x1, x2, target
1. , 2. , 0
4. , 5  , 1
7. ,    , 0
```

首先，创建*TextLineReader*读这个文件。*TextLineReader*打开文件（你指定的）一行一行读。这和变量和队列一样都是有状态的操作；如果多次运行计算图，当前读哪个文件，读到哪个位置，这些状态都会记录保留。

```python
reader = tf.TextLineReader(skip_header_lines=1)
```

下面，我们创建队列，*reader*会从这里拿到接下来要读取的文件。再创建一个入队操作和一个占位器，可以将文件名放入队列，还要创建一个当文件都读完后关闭队列的操作：

```python
filename_queue = tf.FIFOQueue(capacity=10, dtypes=[tf.string], shapes=[()])
filename = tf.placeholder(tf.string)
enqueue_filename = filename_queue.enqueue([filename])
close_filename_queue = filename_queue.close()
```

现在我们准备创建一个读操作，每读一次返回一个键值对。键是记录的唯一标记符  ----包含文件名，冒号（：）和行号 ---- 的字符串。值是简单字符串包含这行的内容：

```python
key, value = reader.read(filename_queue)
```

我们已经准备好一行行读文件了！只差一件事没做好--要把字符串解析成特征（features）和目标（target）：

```python
x1, x2, target = tf.decode_csv(value, record_defaults=[[-1.], [-1.], [-1]])
features = tf.stack([x1, x2])
```

第一行是用TensorFlow CSV解析器抽取出当前行的内容。如果有的域值丢失了会用默认值代替（这个例子中是第三行训练数据**x2**特征），他还能够判断每个域的类型（本例中两个浮点型一个整型）。

最后，我们把训练实例放入*RandomShuffleQueue*这样计算图可以共享，接着我们创建关闭队列的操作，这样等我放好数据可以关掉它：

```python
instance_queue = tf.RandomShuffleQueue(
    capacity=10, min_after_dequeue=2,
    dtypes=[tf.float32, tf.int32], shapes=[[2],[]],
    name="instance_q", shared_name="shared_instance_q")
enqueue_instance = instance_queue.enqueue([features, target])
close_instance_queue = instance_queue.close()
```

Wow！读个文件要做这么多工作。而且我们只是创建图，现在我们需要运行他：

```python
with tf.Session([...]) as sess:
    sess.run(enqueue_filename, feed_dict={filename: "my_test.csv"})
    sess.run(close_filename_queue)
    try:
        while True:
            sess.run(enqueue_instance)
    except tf.errors.OutOfRangeError as ex:
        pass # no more records in the current file and no more files to read
    sess.run(close_instance_queue)
```

首先创建session，入队文件"*my_test.csv*"然后立即关掉队列，因为没有文件要入队了。接着无限循环运行入队操作。*enqueue_instance*依赖reader读取下一行，每个循环都会读进一条新纪录直到文件结束。在这个点*reader*会读队列里的下一个文件，如果队列已经关闭就会抛出`OutOfRangeError`异常（如果队列没有关闭，操作会阻塞知道我们塞入另一个文件或者关闭队列）。最后，我们关掉队列实例这样训练操作就不会一直阻塞。图12-9总结我们学到的东西；他展现了典型的从CSV文件集读训练数据的过程。

![106](/img/设备和服务器上的分布式Tensorflow/106.JPG)

在训练图中，你需要创建共享队列，可以轻松的从其读取mini-batch训练数据：

```python
instance_queue = tf.RandomShuffleQueue([...], shared_name="shared_instance_q")
mini_batch_instances, mini_batch_targets = instance_queue.dequeue_up_to(2)
[...] # use the mini_batch instances and targets to build the training graph
training_op = [...]
with tf.Session([...]) as sess:
    try:
        for step in range(max_steps):
            sess.run(training_op)
    except tf.errors.OutOfRangeError as ex:
        pass # no more training instances
```

这个例子中，第一个mini-batch会包含CSV文件的前两个实例，第二个mini-batch包含最后那个实例。

`TIPS`：TensorFLow队列无法处理稀疏张量，所以如果你训练实例是稀疏的，需要队列实例化后手动解析记录。

这个构架只使用单线程读记录，接着将记录放入队列。如果能使用多个线程同时使用多个*reader*读多个文件,那吞吐量就高多了。我们看看如果做到。

**使用*Coordinator*和*QueueRunner*实现多线程*reader``（Multithreaded readers using a Coordinator and a QueueRunner）`

想要多线程同时读实例，你可以创建*Python*线程（使用**threading**模块）并自己管理。不过，TensorFLow提供了一些工具可以让这些事情简单起来：使用`Coordinator`类和`QueueRunner`类。

`Coordinator`功能非常简单，就是去协调结束多个线程。首先创建**Coordinator**：

```python
coord = tf.train.Coordinator()
```

接着把它传给所有需要一起结束的线程，线程运行的主循环逻辑如下：

```python
while not coord.should_stop():
    [...] # do something
```

任何线程都可以通知其他线程停止，通过调用*Coordinator*的*request_stop()*方法：

```python
coord.request_stop()
```

每个线程都会在完成这次循环后结束运行。你可以通过调用*Coordinator*的*join()*方法等待所有线程结束，这个方法需要传入线程列表：

```python
coord.join(list_of_threads)
```

*QueueRunner*会启动多线程，每个线程都会重复执行入队操作，尽快的填满队列。等队列关闭后，下个还在将将数据放入队列的线程会得到*OutOfRangeError*；这个线程接到错误立刻通过*Coordinator*告诉其他线程停止。下面的代码演示如何使用*QueueRunner*让5个线程同时读数据并放入队列：

```python
[...] # same construction phase as earlier
queue_runner = tf.train.QueueRunner(instance_queue, [enqueue_instance] * 5)
with tf.Session() as sess:
    sess.run(enqueue_filename, feed_dict={filename: "my_test.csv"})
    sess.run(close_filename_queue)
    coord = tf.train.Coordinator()
    enqueue_threads = queue_runner.create_threads(sess, coord=coord, start=True)
```

第一行创建*QueueRunner*并告诉他运行五个线程，都重复执行同样的*enqueue_instance*操作。接着开启session并入队要读的文件名（本例是”my_test.csv“）。接着创建*Coordinator*让*QueueRunner*可以优雅的停止（上面解释过），最后我们告诉*QueueRunner*创建线程并启动。线程会读取所有训练实例并将他们送进队列，而且还能优雅的停止线程。

这种方式比之前的高效一些，但是我们还能做的更好。目前所有的线程读同样的文件，我们果果创建多个*reader*可以让他们同时读不同的文件（假设训练数据在多个CSV文件里），见图12-10。

![107](/img/设备和服务器上的分布式Tensorflow/107.JPG)

为了到达目的，我们要创建*reader*和其他用来读数据和放入队列的操作：

```python
def read_and_push_instance(filename_queue, instance_queue):
    reader = tf.TextLineReader(skip_header_lines=1)
    key, value = reader.read(filename_queue)
    x1, x2, target = tf.decode_csv(value, record_defaults=[[-1.], [-1.], [-1]])
    features = tf.stack([x1, x2])
    enqueue_instance = instance_queue.enqueue([features, target])
    return enqueue_instance
```

接着定义队列：

```python
filename_queue = tf.FIFOQueue(capacity=10, dtypes=[tf.string], shapes=[()])
filename = tf.placeholder(tf.string)
enqueue_filename = filename_queue.enqueue([filename])
close_filename_queue = filename_queue.close()

instance_queue = tf.RandomShuffleQueue([...])
```

最后我们创建*QueueRunner*，这次我们传入一组不同的入队操作，每个操作用不同的reader，这样线程可以同步的从不同文件读数据：

```python
read_and_enqueue_ops = [
    read_and_push_instance(filename_queue, instance_queue)
    for i in range(5)]
queue_runner = tf.train.QueueRunner(instance_queue, read_and_enqueue_ops)
```

运行阶段和前面一样：首先传入要读文件的文件名，接下来创建*Coordinator*，再创建并启动*QueueRunner*线程。现在所有线程就会同时读多个文件了，直到把所有文件读完。最后*QueueRunner*会关掉队列这样其他拉操作就不会阻塞在那了。

**其他便利的方法（Other convenience functions）**

TensorFlow还提供很多方便的方法能够简化读数据的通用任务。我们看看其中一部分（全部的方法请参考API文档）。

*string_input_producer*方法传入包含文件名列表的1D张量，创建一个线程把文件名挨个放入队列，接着关闭队列。如果指定了epochs数量，他会在关闭队列前循环遍历文件名（*it will cycle through the filenames*
*once per epoch before closing the queue*）。默认情况下，每个epoch文件名顺序都会被打乱。他还会创建*QueueRunner*管理线程，并把*QueueRunner*加入`GraphKeys.QUEUE_RUNNERS`集合。想要启动集合里的每个*QueueRunner*，你可以调用*tf.train.start_queue_runners()*方法。注意如果你忘了开启*QueueRunner*，文件名队列会一直打开还空着，你的*reader*也会永远阻塞。

还有其他生产者方法，也是相似的创建队列和相应的*QueueRunner*来运行入队操作（例如，*input_producer()*, *range_input_producer()*和*slice_input_producer()*）。

*shuffle_batch()*方法接收张量列表（比如【特征， 目标】）然后创建：

- *RandomShuffleQueue*
- *QueueRunner*能够将张量放入队列（加入到`GraphKeys.QUEUE_RUNNERS collection`）
- *dequeue_many*操作从队列中一次抽取*mini-batch*数据

这个方法可以让你在单个进程中轻松管理多线程数据输入队列流程，并保证训练流程可以从队列里读mini-batch数据。也请查看*batch()*，*batch_join()*和*shuffle_batch_join()*方法，他们提供相似功能。

OKay！现在你拥有了全部工具，可以开在多设备和服务器上让TensorFlow集群训练和高效的运行神经网络了，我们复习学过了那些内容：

- 使用多个GPU设备
- 设置和启动TensorFlow集群
- 多个设备和服务器上分布式计算
- 使用容器在多个session中共享变量（还有其他有状态的ops比如队列和reader）
- 使用队列支持多个图异步工作
- 使用*reader*，*queue runner*和*coordinator*高效的读输入数据

现在我们使用学过的知识实现并行神经网络！

## 在TensorFlow集群上并行神经网络（Parallelizing Neural Networks on a TensorFlow Cluster）

本节我们先看一下如何通过简单的设置每一个神经网络运行在单独的设备实现并行。接下来我们看看更加棘手的问题，在多个设备和服务器上训练单个神经网络。

### 每个设备一个神经网络（One Neural Network per Device）

最轻松的在TensorFlow集群上训练和运行神经网络的方法，就是和你在同一个机器单个设备跑的一样的代码，创建session时指定一下*master*服务地址，然后就大功告成了！你的代码会泡在服务器的默认设备上。你也可以修改设备，即运行图时候把设备快放进代码里。

并发运行多个客户端session（用不同线程或不同进程），把session连接到不同的服务器，配置他们使用不同的设备，就可以轻松的在所有机器和设备上并行多个神经网络了（图12-11）。提速基本是线性的。在每台2个GPU一共100台机器上训练100个神经网络不会比在1个GPU上训练1个神经网络时间长。

![108](/img/设备和服务器上的分布式Tensorflow/108.JPG)

这个方案对于超参调优是完美的：集群内的每个设备都运行一套属于参数属于自己的模型。运算能力越强，越大的超参空间可以探索。

如果你搭建一个用神经网络预测结果的web服务，每秒会接收大量请求（QPS），那这个方案也是完美的。只需要简单复制神经网络到集群中所有设备，再把请求分发到这些设备就好了。增加更多的服务器意味着接收更多的请求（然而这个方案可不能降低单个请求的等待时间，毕竟需要等待神经网络做预测）。

`TIPS`：另一个选项是使用*TensorFlow Serving*接收请求。这是2016年2月Google发布的开源系统，设计目的为处理对机器学习模型的海量请求。他能够管理模型版本，所以可以轻松在生产环境上部署新版本或不打扰服务的情况下实验多种算法，增加服务器还能承受更大的负载。详细信息请参考https://tensorflow.github.io/serving/。

### 图内复制和图间复制（In-Graph Versus Between-Graph Replication）

如果将每个神经网络放在不同的设备上，你还可以并行训练一个大型集成神经网络（集成学习在第七章介绍过）。然而一旦你想运行集成学习，你需要把每个神经网络不同的预测结果集合起来产生集成学习的预测结果，这部分是需要一定的协调调度。

目前主流的两个处理神经网络集成（或其他包含大量非独立计算的图）的做法：

- 创建一个大图（*one big graph*），包含每一个钉到不同设备的神经网络，，加上集成不同预测结果所需要的计算力（图12-12），接着只需在集群内任一服务器创建session，这就可以搞定一切了（包括集成前等待所有单独预测可用），这种方法叫做图内复制（*in-graph replication*）。

![109](/img/设备和服务器上的分布式Tensorflow/109.JPG)

- 或者，你可以分别为每个神经网络创建图，图之间靠自己同步。这个方法叫做图间复制。一个典型的实现是使用队列协同各个图运算（图12-13）。一组客户端每个处理一个神经网络，从专用的输入队列读数据，写入专用的预测队列。另一个客户端负责读输入数据，把他们推到所有输入队列中（负责所有输入到每一个队列）。最终，最后一个客户会读取每个预测队列中的预测数据并组合起来产生集成预测。

![110](/img/设备和服务器上的分布式Tensorflow/110.JPG)

这些方案都是双刃剑，图内复制（*In-graph replication*）比较简单实现，因为不用管理那么多个客户端和队列。但是，图间复制（*between-graph replication*）比较组织成清晰的，容易测试的模块，而且他还能提供更大的灵活性。比如，你可以在集成客户端加个出队超时（*dequeue timeout*），这样即使其中一个神经网络客户端挂了或者太慢了，最后的预测也不会失败。TensorFlow可以让你在调用run()方法时指定超时，只需传入一个有*timeout_in_ms*的*RunOptions*：

```python
with tf.Session([...]) as sess:
[...]
    run_options = tf.RunOptions()
    run_options.timeout_in_ms = 1000 # 1s timeout
    try:
        pred = sess.run(dequeue_prediction, options=run_options)
    except tf.errors.DeadlineExceededError as ex:
        [...] # the dequeue operation timed out after 1s
```

另外一个办法是设置session的*operation_timeout_in_ms*选项。但是这样的话*run()*方法内的任何操作执行超过超时时间，*run()*方法就会超时：

```python
config = tf.ConfigProto()
config.operation_timeout_in_ms = 1000 # 1s timeout for every operation

with tf.Session([...], config=config) as sess:
    [...]
    try:
        pred = sess.run(dequeue_prediction)
    except tf.errors.DeadlineExceededError as ex:
        [...] # the dequeue operation timed out after 1s
```

### 模型并行（Model Parallelism）

目前为止我们都是在单独的设备上分别训练神经网络。如果我们想在多个设备上运行单个神经网络怎么办？这就需要把模型切分成不同的片段运行到不同的设备上。这就叫做模型并行。不幸的是，模型并行被证实是非常棘手的，而且还特别依赖你的神经网络构架。这种方法对于全连接网络来说没什么提升（图12-14）。直觉上也许可以哟个简单办法可以把模型分割到每个设备一个layer，但其实这不可行，因为每个layer都需要前一个layer的输出才能做事。所以也许可以垂直切割----比如，每个layer左边的部分放在一个设备，右边放到另一个？这明显好很多，因为这样两边都可以并发工作，但是问题是下一层的每一半都需要这层的两部分。这就需要大量的设备间通信了（如箭头展示）。因为设备间通讯非常慢（特别是跨不同主机的），这就彻底的抵消了并行计算的好处。

![111](/img/设备和服务器上的分布式Tensorflow/111.JPG)

然而，我们会在十三章看到，一些神经网络结构，比如卷积网络，layer之间只有部分连接，所以更容易高效的分布式运行在多个设备上。

![112](/img/设备和服务器上的分布式Tensorflow/112.JPG)

此外，我们会在14章看到，一些深度循环神经网络（DRNN）会由很多层的记忆单元组成（图12-16左）。一个单元在时间t的输出会成为t+1的输入（图12-16会看的更清楚）。如果水平切分网络，把每个layer放在不同的设备上，然后第一步一个设备激活，第二步两个，随着信号向后传播到输出layer，所有设备全都同时激活。虽然这样还会有很多设备间的通信，但是因为每个单元可能相当复杂，并发运行多个单元的好处可能远超通讯的负担。

![113](/img/设备和服务器上的分布式Tensorflow/113.JPG)

简言之，几种神经网络可以通过模型并发训练提速，但要小心不是全部网络都可以，即使可以要需要特殊的检查和调整，比如要确保让最多通信的设备运行在同台机器上。

### 数据并行（Data Parallelism）

另一个并行训练神经网络的方法时复制在各个设备副本上，在所有副本上用不同的*mini-batch*同步运行训练步骤，最后集合所有梯度去更新模型参数，这就叫做数据并行（*Data Parallelism*）（如图12-17）。

![114](/img/设备和服务器上的分布式Tensorflow/114.JPG)

这个方法有两种变体：同步更新（*synchronous updates*）和异步更新（*asynchronous*
*updates*）。

**同步更新（*Synchronous updates*）**

使用同步更新，集合器（*aggregator*）等所有副本的梯度算好，再计算平均值，最后得到结果（比如用算好的梯度更新模型参数）。一个副本算好之后必须等参数（*parameters*）更新后才能开始下轮*mini-batch*。缺点是如果一些副本比其他的慢，那大家每一步都要等这些副本算好才能继续下一步（梯度更新后）。这就可能导致参数服务器宽带饱和。

`TIPS`：想要每步降低等待时间，你可以忽略最慢那几个副本（比如~10%）。比如你运行20个副本，每次只计算最快的18个副本值，忽略最慢的两个。参数更新好，前18个副本就可以立即工作了，不用等最慢的两个。这个过程被叫做*18 replicas plus 2 spare replicas*。

**异步更新（*Asynchronous updates*）**

异步更新是指，只要一个副本计算好梯度，直接用他来更新模型参数，不做集成了（去掉图12-17种的”**mean**“步骤），也不用同步。副本之间相互独立。既然不用等其他副本，那每分钟就能跑更多步骤。此外，虽然参数还需要复制到所有设备上，但是因为发生时间不同，宽带饱和的问题也会减缓。

数据异步并行跟新是个有吸引力的选择，因为即简单，又没有异步延迟，还能更好利用带宽。然而，尽管实践上他运行的不错，但是真的很奇怪他怎么会做的不错呢！实际上，当一个副本根据一些参数算好梯度后，这些参数还会被其他副本更新好多次（如果是N个副本，那平均N-1次更新），所以无法保证计算好的梯度还能指着正确的方向啊（图12-18）。如果梯度严重过期，这就叫做腐败的梯度（*stale gradients*）：这会减慢收敛，引入噪音还有摇摆效应（*wobble effects*）（学习曲线有暂时的震动），甚至会导致训练算法发散（*diverge*）。

![115](/img/设备和服务器上的分布式Tensorflow/115.JPG)

有一些办法可以减少腐败梯度（*stale gradients*）效应：

- 降低学习率（*learning rate*）
- 丢掉或缩小腐败梯度
- 调整*mini-batch*大小
- 在最开始几个epoch只使用一个副本（这叫做热身阶段）。腐败梯度问题在训练开始时期伤害特别大，因为这时候梯度特别大而参数还没有被代价函数限制住。所以不同的副本会把参数带到不同方向上。

2016年四月Google Brain团队发布一篇[论文](http://goo.gl/9GCiPb)称，通过多种测试发现，数据并行同步更新加少量备用副本的方式效能最佳，不仅收敛最快而且产生模型更好。不过这部分还是研究的活跃领域，不好贸然下结论。

**宽带饱和（*Bandwidth saturation*）**

无论是同步或异步更新，数据并行里每个副本在开始和结束一次训练时还是需要和参数服务器通信两次，不幸的是，这意味这加GPU提速没用因为时间花在了搬进搬出GPU RAM了（还有可能网络间），这有可能把分解计算负荷的努力抵消了。在这个时候，增加GPU只会加重饱和并减慢训练速度。

`TIPS`：对于某些相对较小但训练数据很大的模型，用单机单GPU训练通常更好。

饱和在大密度模型上问题更严重，因为他有大量的参数和梯度要传输。对于小模型影响要小（不过小模型也不需要并行了），对稀疏模型影响也小，因为大部分参数和梯度都是0，所以通信很高效。Google Brain的领导Jeff Dean[指出](https://www.youtube.com/watch?v=WjU-s4_oJ94)，用50个分布式GPU训练的的密集模型（*dense models*）能提速25-40倍，稀疏模型用500个GPU可以提速300倍。你看稀疏模型可以扩展的很好。下面是一些具体例子：

- **Neural Machine Translation**：8GPU提速6倍
- **Inception/ImageNet**：50GPU提速32倍
- **RankBrain**：500GPU提速300倍

这些数字代表了2016年Q1的最高水准，包含了几十个GPU训练密集模型，几百个GPU训练稀疏模型，各种饱和还有服务降级。现在还有大量的研究在努力解决这个问题（探索更多的P2P构架而不是参数集中式构架，使用有损模型压缩，副本通信优化等等），所以今后几年并行神经网络很有可能会有很大进步。

同时，你还可以采用一些简单的步骤减少饱和问题：

- 把你的GPU集中放在少量服务器上而不是散布在很多服务器上，这会避免无用的网路通讯损耗。
- 在多个参数服务器上参数分片（上面讨论过）。
- 将模型参数类型精度从浮点32（`tf.float32`）降到浮点16（`tf.float16`）。这会让传输数据减少一半，而不会影响太多收敛率或模型性能。

`TIPS`：尽管16位精度是训练神经网络的最低要求，你实际上还是可以把训练好的模型精度降低到8位，这样能减小模型的大小并计算提速。这叫做量化（*quantizing*）神经网络。这招在部署和运行在手机上的预训练模型（*pretrained model*）上特别有用。这个主题请看Pete Warden’s的[帖子](https://petewarden.com/2016/05/03/how-to-quantize-neural-networks-with-tensorflow/)。

**TensorFlow实现（*TensorFlow implementation*）**

想要用TensorFlow实现数据并行，首先要选择你想用图内复制（*in-graph replication*）还是图见复制（between-graph replication）。还要选择同步更新还是异步更新。咱们看看如何实现各种组合（看练习和Jupyter笔记上的完整代码）。

想要图内复制 + 同步更新，你要建立一个大图包含所有模型副本（放在不同设备上），还有一些节点来集合所有梯度，再把他们喂给优化器。你的代码在集群上打开session，轻松的重复运行训练操作。

想要图内复制 + 异步更新，同样创建一个大图，不过每个副本一个优化器，然后一个线程运行一个副本，重复运行副本的优化器。

想要图间复制 + 同步更新，运行多个独立客户端（在各自的进程里），训练每个模型副本就像他是孤立的，但是其实参数在不同副本上是共享的（使用资源容器）。

想要图间复制 + 异步更新，还是运行多个独立客户端，基于共享参数训练每个模型副本，但是这次要用`SyncReplicasOptimizer`包装优化器（比如`MomentumOptimizer`）。每个副本就像用其他优化器那样用这个优化器，但优化器底层其实把梯度送到一组队列里（每个变量一个），一个副本的`SyncReplicasOptimizer`会去读这些队列，这个优化器叫首席（`chief`）。首席会集合梯度并计算它们，接着给每个副本的token队列写个token，就算提醒副本们可以计算下个梯度了，这个方法是支持有备用副本（`spare replicas`）的。

如果你完成练习作业，你要实现这四种解决方案。你会轻松的把训练深度神经网络应用到许许多多GPU上！下面的章节我们将学习一些更重要的神经网络构架，再下来我们看看增强学习。

(我很喜欢这部分内容，所以把翻译出来，时间有限，翻译质量一般，抱歉。)