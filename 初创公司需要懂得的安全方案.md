# 初创公司需要懂得的安全知识

> 本文翻译自[Security for Startups – What you need to know](https://breachinsider.com/blog/2018/security-for-startups-what-you-need-to-know/)，我过了一遍感觉清晰有效，做成**checklist**也很棒，翻译出来贡大家参考。

在初创公司里，大部分时间内我们忙于深入思考业务和公司成长，却忘记考虑员工，基础设施或代码的安全。通常先把安全放一边是有必要的，但是千万不能完全忽略他。如果忽略安全，公司会成为隐藏在我们中间坏蛋的诱人目标，越大的成长意味着更多用户数据被偷窃。

本文会列出一些关键点和解决方法，会帮助你降低风险，不会那么容易成为下一次数据泄露的靶子。

**目录**

[TOC]

## 目标读者

本文适于创始人，CTO或VP。部分内容适合开发者，但是本文不会深入讨论烂代码导致web应用被攻击此类问题。

## 保护你和你的员工

人类很不幸总是默默为黑客带路，因为好奇的天性和奇怪的冲动，总喜欢点这点那的。看看我们能做点什么让这种事少发生...

### 每个人都要关心安全

**A problem shared is a problem halved**，让每个人思考自己的安全问题，这样他们的行动会影响公司的安全。比如，贴上公司主管的email地址可能让你收到所有有用的反馈，但也会让他们成为垃圾/恶意邮件（或者更糟的钓鱼邮件），虽然公开邮箱没错，查看邮箱的人必须能用区分恶意邮件。

特别是那些和客户打交道的角色，他们始终处于风险之中，所以必须花时间做安全意思培训（**Udemy**有很多免费的好课程）。

### 2FA Everything

双重认证（Two factor authentication）意思是登录时不但要输入用户名&密码，还要再通过其他手段，比如一次性code或交互式的推送通知。

创业公司里所以使用的账户都应该尽可能使用2FA。为什么？因为就算有人破解了你的用户名和密码，没过二阶段验证，他们依然无法进入你的账户。所以如果你的供应商或者你试用的创业公司有数据泄露，你的用户名密码暴露在公众视野下，被拿到各个网站探测，他们依然会失败（当然他们试都不会试，你不会只用一个密码的对吗？）。

- 如果你是Google G Suite类型的公司：[整个domain强制2FA](https://support.google.com/a/answer/184711)
- Office 365? [没问题，全部被cover](https://support.office.com/en-us/article/set-up-multi-factor-authentication-for-office-365-users-8f0454b2-f51a-4d9c-bcde-2c48e41621c6)
- Cloudflare上的domain？[不要让任何人hijack你整个domain]([Don't let anyone hijack your entire domain!](https://support.cloudflare.com/hc/en-us/articles/200167866-How-do-I-set-up-two-factor-authentication-))
- Slack？[放心，我们不会让全世界知道你工作午餐订单](https://get.slack.help/hc/en-us/articles/212221668-Mandatory-workspace-two-factor-authentication-)
- Github. [因为，你懂得... 源代码也非常重要](https://help.github.com/articles/requiring-two-factor-authentication-in-your-organization/)

### 密码管理器

如果可能，让每个人都用密码管理器管理各自账户（[KeePass](https://keepass.info/) & [PasswordSafe](https://pwsafe.org/)都是免费开源产品）。类似上面2FA的情况，当你的供应商或第三方工具泄露，有一个你的员工的密码是清晰的'+1'模式。那别人不会话他长时间就会摸到他GitHub等网站的密码了。

密码管理器提供了自动填充密码的功能，这也会减少网络钓鱼的机会。如果你的员工收到了钓鱼邮件并被引导到钓鱼网站，密码管理器不会填充密码，这会提醒你：”是不是哪里怪怪的“。

### 共享账户

在理想世界，这种情况不会发生，但事实不是这样，他们这么做反而是想保护这些账户。最佳方案是有一个专门的团队管理密码（1Password Teams是个选项），每个人用同一张诗片唱歌。单一数据源包装你可以快速跟踪每个人可以访问哪，密码在什么时间重置或修改过。这非常重要，特别是当员工离职，共享的账户需要立刻升级，越快越好。

无论如何管理账户，别忘了保存所有共享账户，相关密码和最后更新时间的日志。

### 加密

考虑加密保护所有员工的设备 - 台式/笔记本电脑要全磁盘加密，移动设备使用强密码锁屏（用TouchID的指纹授权更好）。

当遇到不可避免的情况，比如某人把包包或手机遗留在酒吧或火车上，这种加密肯定有用。同样这会降低意外事故造成的损失，比如孩子的小手或好奇的朋友。

全磁盘加密现在已经非常普遍了。对大多数系统几分钟就能启用。

- [Filevault](https://support.apple.com/en-gb/HT204837) Apple可用
- [BitLocker](https://uit.stanford.edu/service/encryption/wholedisk/bitlocker) Windows可用
- [A whole host of options](https://wiki.archlinux.org/index.php/disk_encryption) Linux可用 - 考虑 [dm-crypt and LUKS](https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system)作为起点

## 保护你的基础架构

基础架构很可能是最吸引别人攻击的目标，他们会想通过这些设施找到贵公司的潜在漏洞。你的服务器是24/7/365在线（希望），并且很可能一直连着internet，这意味着黑客和安全专家会不停的扫描开放端口。因此，定期确保基础架构是尽可能封闭的非常重要。

### 持续跟踪基础架构

对某些人来说这是显而易见的，但对保护你的资产/设施非常重要。没有这个清单，当下次[Heartbleed](http://heartbleed.com/)出现，你却不知道哪些设备需要升级或打patch。好的服务器清单要包含如下内容：

- 都在跑什么应用（web服务器，缓存，后台应用？）
- hosted在哪（办公室，众多云服务商的一个？）
- OS是什么（Debian, Ubuntu, Windows...)
- 公开地址s

### Web服务器TLS

为了你的用户和品牌，使用TLS让他们和你服务器的链路保持加密。**HTTPS**变得越来越受重视，现在的浏览器遇到**HTTP**网站都会显示不安全的图标，这很不好看。

TLS证书也不需要花钱，实际上你可以通过[Lets Encrypt](https://letsencrypt.org/)免费使用，他可以提供有效证书，90天过期。这会鼓励你自动续期证书（用软件很容易做到）。而不会因为使用过期证书而烦恼。

### Patch不能断

如果管理自己的设备或服务器，包装他们是最新的。这包含OS补丁，应用和你使用的所有库/依赖。

有一种倾向是只在绝对需要的时候才打补丁（比如一个关键漏洞被公开披露），然而这种做法会导致更深的问题。当为重要问题打补丁或修补关键风险时，你当然希望部署越快越干脆越好。如果背后有多个版本，那不但要考虑向后兼容问题，还要结合快速部署代码修正新bug的问题（不会翻译这句：If you’re many versions behind, you not only have to deal with non-backwards compatibility issues, you also compound the issue by having to deploy sudden fixes to your code to fix these new bugs.）。

- [Unattended-upgrades](https://wiki.debian.org/UnattendedUpgrades) 基于Debian或Ubuntu的服务器
- [yum-cron](https://access.redhat.com/discussions/1238193) 基于Redhat和CentOS
- [dnf-automatic](https://fedoraproject.org/wiki/AutoUpdates) Fedora
- [Windows Updates](https://docs.microsoft.com/en-us/windows-server/administration/windows-server-update-services/deploy/4-configure-group-policy-settings-for-automatic-updates) Windows

### 每个地方都要强认证

- 一定要改掉默认证书，一定要哦。确保使用上面说到的密码管理器，生成的密码要大于20个字符。感谢密码管理器，你不用背下来这些密码。
- 如果可以（SSH），用[key-based authentication](https://kb.iu.edu/d/aews)保证只有正确私钥才能成功登陆。注意如果使用基于key的认证，别忘了禁用密码提示。

### 保护内部服务器私密

如果基础构架内有些机器不需要连接互联网，那使用云服务内的网路服务保证这些机器和互联网隔绝。

- Amazon里叫做[Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/)
- Google里也叫做[Virtual Private Cloud (VPC)](https://cloud.google.com/vpc/)
- DigitalOcean里叫做[Private Networking](https://www.digitalocean.com/docs/networking/private-networking/overview/)

如果你想知道如果不能连接互联网怎么访问这些机器呢（比如为了维护直接访问数据库），只需要设立一台低配服务器扮演**跳机**的角色。跳机有公网IP可以访问。这台主机必须只能是基于key的SSH授权。理想情况下这台机器最好只有在必须使用的时候才开机。

Windows和RDP呢？[Tunnel RDP over SSH](https://superuser.com/a/979333)，这样就不用暴露端口3389到公网等着人家破解了。

### 日志集中

出了问题查原因，或者跟踪一个很难复现的bug的时候，日志就非常重要了，其余时间日志反而会被忽略。

安全事故出现后，必须要思考what，when，where和who来分析问题。如果日志只在本地服务器，他们就有可能被攻击者篡改，或者直接被服务器（或应用）删除。通过安全的传输日志到集中的地方，日志就能被保留和监控。

- 如果使用云服务（AWS, Google)，考虑他们自建的日志平台。
- 如果是自搭建平台，可以考虑[ELK Stack](https://www.elastic.co/elk-stack)或[Graylog](https://www.graylog.org/)，这些都很棒，文档也很完整。

## 保护你的品牌

不单单要考虑员工和基础构架 - 有时你也需要考虑那些无法完全掌控的关键服务。

### 保护你的域名

域名就是你的全部 - 你的品牌，你的网站，你的邮箱等等。所以你要做的肯定不单单是每年续费。

如果能选择，你应该不仅仅根据年费价格来选一个域名服务商，而是根据账户的信任程度，安全等级来选择。

- 在域名注册商帐户上使用强密码
- 如果可以使用双重认证（2FA） - 最好用一定时间段有效的密码（one-time passwords， TOTP），可以用[Google Authenticator](https://support.google.com/accounts/answer/1066447?co=GENIE.Platform%3DAndroid&hl=en)或[Microsoft Authenticator](https://www.microsoft.com/en-us/account/authenticator)。
- 域名续约时间越长越好，把续约时间放到你的日历里。

### 保护你的第三方服务

和上面类似，你应该施与相同的等级去审核和保护你的关键第三方应用和服务。你的商业数据泄露不一定发生在你自己的服务器上，更可能客户和私人信息被多个其他商业服务共享。比如不太可能你自己实现email平台 -- 任何商业的生命和灵魂 -- 所以保证这个服务很难被黑很关键。所以好声誉的服务提供方都会通过重复认证和长且复杂的密码保护你的账户。

- [How to harden Google Apps (GSuite)](https://blog.trailofbits.com/2015/07/07/how-to-harden-your-google-apps/)
- [Office 365](https://support.office.com/en-gb/article/security-best-practices-for-office-365-9295e396-e53d-49b9-ae9b-0b5828cdedc3)
- 支付流程包含非常有趣的信息。[Stripe](https://support.stripe.com/questions/enable-two-step-verification)
- 类似Saleforce那样的CRMs也包含一个教你如何保护隐私的宝藏。[Securing Salesforce](https://developer.salesforce.com/docs/atlas.en-us.securityImplGuide.meta/securityImplGuide/salesforce_security_guide.htm)

### 发现你的数据泄露

防止数据泄露是非常困难的，因为能让他发生的办法太多了。防止软件和服务出问题需要大量的资源，不仅构建，还包括维护。通常需要安排专门一个团队监控处理这些警告，但对于敏捷的初创公司这太难负担了。如果创建和维护服务操作不正确，还会产生大量不靠谱的假警告，到处都是’狼来了‘。

这就是为什么我们在[Breach Insiders](https://breachinsider.com/)上决定使用一种不同的方法，即**侦测**而非防堵。即我们可以提供各种数量的不同用户信息，放到你的数据库，CRMs和newsletters里，这些用户都有真实邮件和电话，如果被联系上（不是你的商务用户）立刻报警让你调查。额外的好处是因为这些用户是独立于你的商务，你可以不断地扫描**the Internet and Dark Web (Tor)**去找到他们，找到符合的后可以立刻警告他们。

### 开诚布公

展示清晰而简单的条款，诚实的告诉你的客户你收集什么样的数据（现在EU的GDPR法律已经要求你这么做了）。严谨的告诉客户你采用的安全手段，甚至应该向大众公布你内部的安全策略（简单版也可以）。

强烈建议你考虑设置一个联系方式专门处理安全问题，这样研究人员可以轻松联系到你，告诉你他们发现的业务安全问题。最简单的办事就是设置**security@*your-domain.com***邮箱。

通过开诚布公的方式，不但可以为大众建立好印象，同时也容易打消黑客攻击你的念头。

## 保护你的应用和代码

数据泄露是一个’**什么时候发生**‘的问题而不是’**如果会发生**‘的问题。等到发生了，如果你的应用和代码是安全的，可以最小限度限制破坏发生后的影响。

### 为密码选择强大和众人推荐的哈希算法，要用salt

糟糕差劲的哈希算法会让密码容易被暴力破解。

最糟的犯罪做法是用普通文本保存密码，不用哈希 - **永远不要用普通文本存密码**。MD5和SHA1是下个级别，这种方法也可以通过彩虹表被逆推出来。使用随机salt+password会让上面的破解不再有效，不过如果使用强劲的CPU或多个GPU还是能很快暴力破解出来。

[brilliant OWASP Foundation](https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet#Leverage_an_adaptive_one-way_function)推荐使用如下方案，按优先顺序排列：

- Argon2
- PBKDF2
- scrypt
- bcrypt

### 永远不要保存付款详细信息

如果不严格按照[Payment Card Industry Data Security Standard (PCI DSS)](https://en.wikipedia.org/wiki/Payment_Card_Industry_Data_Security_Standard)要求存储付款详情，那会让你陷入各种麻烦当中。如果有外部服务能遵守这些法规并正确存储和管理敏感信息，那交给他们比自己做容易多啦。

### 监控你的依赖

很有可能你的初创服务依赖于第三方或开源的库和软件。保证这些依赖一直是最新版本很重要，因为里面会有重要的安全更新。举个依赖库的例子，**Django** - 流行的Python web服务，或者Python, Ruby或NodeJS的`json`解析库。

### 保证你的secrets私密

保证你的secrets -- 比如私钥，密码，api证书 -- 严格受管控。包括开发人员使用的环境而不只是生产环境（比如测试环境）。

这很重要，因为他可以让人无限的访问你的公司和你的系统。常见的例子是api keys放在代码里提交到代码库了（例如Github），那就有可能别其他人看到。由于这些服务的特性，修改这个问题很麻烦，比如另外一个提交删除了敏感字符串，但是这个敏感字符串实际没有被删除，他还在这个文件的历史版本里。

在理想的世界里，如果你的Github账户被滥用或者破解了，那最多只能看到源代码。但如果secrets被提交了（即使只有一次），那他们会扫描代码利用这些字符对你的系统做更深入的事情。

- 可以考虑使用AWS Labs提供的 [git-secrets](https://github.com/awslabs/git-secrets)，防止开发者提交敏感字符串。

### 为你的用户双重认证（2FA）

不要忘记要让你的用户也能自己保护自己。通过实现双重认证，你允许他们掌控自己的安全问题，同时因为展示了对安全问题严肃认真的态度，这也提升你的品牌形象。对于一些用户，你和竞争者间区别就是例如2FA这样的’小‘功能。

很多的web框架有2FA插件：

- [Django Two Factor Auth](https://github.com/Bouke/django-two-factor-auth)
- [Ruby on Rails - Devise Two Factor](https://github.com/tinfoil/devise-two-factor)
- [Laravel Google2FA](https://github.com/antonioribeiro/google2fa-laravel)
- [SpeakEasy for NodeJS](https://github.com/speakeasyjs/speakeasy)

